FROM node:10 as builder
COPY . .
RUN npm install &&\
    npm ci --only=production &&\
    npm run build

FROM nginx:alpine
COPY --from=builder /dist /usr/share/nginx/html